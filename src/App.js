import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import routes from 'routes/index';

import 'containers/style.less';
function App() {
  return (
    <BrowserRouter>
      <Switch>
        {/* <div>a</div> */}
        {/* <Route path="/404" render={props => <Page404 {...props} />} /> */}
        {/* <Route path="/login" render={props => <Login {...props} />} /> */}
        {/* <Route path="/" render={props => <DefaultLayout {...props} />} /> */}
        {routes.map((route, idx) =>
          route.component ? (
            <Route
              key={idx}
              path={route.path}
              exact={route.exact}
              render={(propsComponent) => (
                <route.component {...propsComponent} />
              )}
            />
          ) : null,
        )}
      </Switch>
    </BrowserRouter>
  );
}

export default App;
