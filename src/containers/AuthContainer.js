import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Button, Menu, Dropdown, DatePicker, Rate } from 'antd';
import 'antd/dist/antd.css';
const AuthContainer = () => {
  const [active, setActive] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      setActive(false);
    }, 5000);
  }, []);

  const menu = () => {
    return (
      <Menu>
        <Menu.Item>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="http://www.alipay.com/"
          >
            1st menu item
          </a>
        </Menu.Item>
      </Menu>
    );
  };

  return (
    <div>
      <div>Form Login</div>
      <Button type="primary" loading={active}>
        Loading
      </Button>

      {active && (
        <Dropdown overlay={menu}>
          <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
            Cascading menu
          </a>
        </Dropdown>
      )}
      <DatePicker renderExtraFooter={() => 'extra footer'} showTime />

      <Rate
        defaultValue={2}
        character={({ index }) => {
          return index + 1;
        }}
        onChange={(id) => {
          if (id === 5) {
            setActive(true);
          } else {
            setActive(false);
          }
        }}
      />
    </div>
  );
};

export default AuthContainer;
