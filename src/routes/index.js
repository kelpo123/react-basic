import HomeContainer from '../../src/containers/HomeContainer';
import AuthContainer from '../../src/containers/AuthContainer';
const routes = [
  {
    path: '/',
    exact: true,
    component: HomeContainer,
  },
  {
    path: '/auth',
    exact: true,
    component: AuthContainer,
  },
];

export default routes;
